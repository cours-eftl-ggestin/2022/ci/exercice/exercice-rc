package javabean;

public class Programme {

	public static void main(String[] args) {
		Article monArticle = new Article();
		monArticle.setLibelle("Article 1");
		monArticle.setNumero(1);
		
		ArticleService articleService = new ArticleService();
		articleService.creerArticle(monArticle);
		
	}

}
