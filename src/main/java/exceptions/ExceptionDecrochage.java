package exceptions;

public class ExceptionDecrochage extends Exception {
	
	private Integer vitesse;
	
	public ExceptionDecrochage(Integer vitesse) {
		this.vitesse = vitesse;
	}
	
	public String getRaison() {
		return "Changez l'inclinaison ou augmentez la vitesse. Vitesse actuelle = " + this.vitesse;
	}

}
