package exceptions;

public class Tableaux {

	private String[] tableau = {"Ain", "Aisne", "Allier"};
	
	public void afficheTableau() throws Exception {
		for (int i = 0; i<5; i++) {
			if (i > tableau.length-1) {
				throw new Exception("Indice en dehors des bornes du tableau : " + i);
			}
			System.out.println(tableau[i]);
		}
	}
}
