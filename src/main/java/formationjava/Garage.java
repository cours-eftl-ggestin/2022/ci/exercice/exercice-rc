package formationjava;

public class Garage {
	
	public static String adresse = "Rue des Tests";

	public static void main(String[] args) {
		Voiture voiture1 = new Voiture();
		voiture1.setMarque("Renault");
		voiture1.rouler();
		
		Voiture voiture2 = new Voiture();
		voiture2.setMarque("Peugeot");
		voiture2.rouler();
		System.out.println(voiture2.getMarque());
		
		System.out.println("La voiture de marque " 
				+ voiture2.getMarque() 
				+ " est entretenu par le garage situé " + Garage.adresse);
		Garage monGarage = new Garage();
		String monAdresse = monGarage.adresse;
	}

}
