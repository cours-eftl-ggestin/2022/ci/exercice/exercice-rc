package heritage;

public class Camion extends Vehicule {
	
	public Camion(Boolean type) {
		super(type);
	}
	
	public void rouler() {
		System.out.println("Le camion roule!");
	}

}
