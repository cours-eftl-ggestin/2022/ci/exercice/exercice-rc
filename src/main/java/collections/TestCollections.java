package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class TestCollections {

	public static void main(String[] args) {
		
		List<String> maListe = new ArrayList<>();
		
		maListe.add("valeur1");
		maListe.add(1, "valeur2");
		maListe.add(0, "valeur3");
		maListe.add("valeur1");

		List<String> monAutreListe = new ArrayList<>();
		monAutreListe.addAll(maListe);
		System.out.println("Le deuxième élément de ma liste est : " + maListe.get(1));
		
		int indice = maListe.indexOf("valeur1");
		System.out.println("Le premier indice pour valeur1 est : " + indice);
		int dernierIndice = maListe.lastIndexOf("valeur1");
		System.out.println("Le dernier indice pour valeur1 est : " + dernierIndice);
	
		System.out.println("La valeur 'valeur2' existe ? " + maListe.contains("valeur2"));
		System.out.println("La valeur 'valeur4' existe ? " + maListe.contains("valeur4"));
		
		/*System.out.println("Taille de la liste = " + maListe.size());
		maListe.remove(0);
		System.out.println("Taille de la liste = " + maListe.size());
		maListe.remove("valeur1");
		System.out.println("Taille de la liste = " + maListe.size());
		maListe.clear();
		System.out.println("Taille de la liste = " + maListe.size());*/
		
		Set<String> monSet = new HashSet<>();
		monSet.addAll(maListe);
		for (String element : monSet) {
			System.out.println("Elément courant dans le set = " + element);
		}
		
		String[] valeurs = {"un", "deux", "trois"};
		List<String> maNouvelleListe = (List<String>) Arrays.asList(valeurs);
		
		List<String> maListeNonTriee = new ArrayList<>();
		maListeNonTriee.add("vaucluse");
		maListeNonTriee.add("allier");
		maListeNonTriee.add("loire");
		Collections.sort(maListeNonTriee);
		for (String element : maListeNonTriee) {
			System.out.println(element);
		}
		
		
		List<Planete> listeDesPlanetes = new ArrayList<>();
		listeDesPlanetes.add(new Planete("Mercure", 6000, 50));
		listeDesPlanetes.add(new Planete("Mars", 5000, 200));
		listeDesPlanetes.add(new Planete("Jupiter", 11000, 1100));
		
		Comparator<Planete> comparateurDeTaille = new Comparator<>() {
			public int compare(Planete planete1, Planete planete2) {
				return planete1.taille.compareTo(planete2.taille);
			}
		};
		
	
		Collections.sort(listeDesPlanetes, comparateurDeTaille);
		
		Collections.sort(listeDesPlanetes, new ComparateurPlanetes());
		
		System.out.println("Parcours de la liste avec itérateur");
		Iterator<String> monIterateur = maListe.iterator();
		while (monIterateur.hasNext()) {
			String element = monIterateur.next();
			System.out.println(element);
		}
		
		System.out.println("Parcours de la liste avec boucle for classique");
		for (int monIndice = 0;monIndice < maListe.size();monIndice++) {
			String monElement = maListe.get(monIndice);
			System.out.println(monElement);
		}
		
		System.out.println("Parcours de la liste avec boucle for intelligente");
		for (String elem : maListe) {
			System.out.println(elem);
		}
		
		System.out.println("Parcours de la liste avec méthode forEach");
		maListe.forEach(e -> {System.out.println(e);});
		
		Stream<String> monStream = maListe.stream();
		monStream.forEach(e -> {System.out.println(e);});
		
		List<Integer> listeInteger = Arrays.asList(3, 9, 1, 4, 7, 2, 5, 3, 8, 9, 1, 3, 8, 6);
		System.out.println("listeInteger : " + listeInteger);
		Set<Integer> setInteger = new HashSet<>(listeInteger);
		System.out.println("setInteger : " + setInteger);
		setInteger.add(5);
		System.out.println("setInteger après add 5 : " + setInteger);
		
		List<String> listeString = Arrays.asList("toto", "abc", "xyz", "dede", "abc");
		System.out.println("listeString : " + listeString);
		Set<String> setString = new HashSet<>(listeString);
		System.out.println("setString : " + setString);
		
		Map<Integer, String> maMap = new HashMap<>();
		maMap.put(1, "Mercure");
		maMap.put(2, "Vénus");
		maMap.put(3, "Terre");
		
		for (Map.Entry<Integer, String> entry : maMap.entrySet()) {
			System.out.println("Clé = " + entry.getKey() + "; Valeur = " + entry.getValue());
		}
		
		System.out.println("Parcours avec forEach :");
		maMap.forEach((cle,valeur) -> { System.out.println("Clé = " + cle + " ; Valeur = " + valeur);});
		
		Map<Integer, Planete> mesPlanetes = new HashMap<>();
		mesPlanetes.put(1, new Planete("Mercure", 5000, 40));
		mesPlanetes.put(2, new Planete("Vénus", 3000, 150));
		
		for (Map.Entry<Integer, Planete> entry : mesPlanetes.entrySet()) {
			System.out.println("Clé = " + entry.getKey() + "; Valeur (nom) = "+ entry.getValue().nom);
		}
		
		System.out.println("Parcours avec forEach :");
		mesPlanetes.forEach((cle,planete) -> { System.out.println("Clé = " + cle + " ; Valeur (taille) = " + planete.taille);});
		
		Map<Integer, List<Planete>> mesListesDePlanetes = new HashMap<>();
		mesListesDePlanetes.put(1, listeDesPlanetes);
		
		List<Planete> listeDesPlanetesNum2 = new ArrayList<>();
		listeDesPlanetesNum2.add(new Planete("Venus", 6000, 50));
		listeDesPlanetesNum2.add(new Planete("Terre", 5000, 200));
		listeDesPlanetesNum2.add(new Planete("Uranus", 11000, 1100));
		
		mesListesDePlanetes.put(2, listeDesPlanetesNum2);
		
		List<Planete> laListeDesPlanetesPourCle2 = mesListesDePlanetes.get(2);
	}

}
