package collections;

public class Planete {
	
	public String nom;
	public Integer taille;
	public Integer distance;
	
	public Planete(String nom, Integer taille, Integer distance) {
		this.nom = nom;
		this.taille = taille;
		this.distance = distance;
	}

}
