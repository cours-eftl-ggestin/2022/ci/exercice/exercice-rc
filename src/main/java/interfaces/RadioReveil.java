package interfaces;

public class RadioReveil implements IRadio, IReveil {
	
	private Boolean typeReveil;
	
	public RadioReveil(Boolean typeReveil) {
		this.typeReveil = typeReveil;
	}
	
	public void start() {
		if (typeReveil) {
			IReveil.super.start();
		} else {
			IRadio.super.start();
		}
	}
	
	public void setHeure(String heure) {
		
	}
	
	public void setVolume(Integer volume) {
		
	}

}
