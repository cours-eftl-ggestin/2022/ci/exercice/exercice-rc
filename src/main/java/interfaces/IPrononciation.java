package interfaces;

public interface IPrononciation {

	public String prononce(String mot);
}
