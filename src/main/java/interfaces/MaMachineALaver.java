package interfaces;

public class MaMachineALaver implements IMachineALaver {
	
	private Boolean fonctionnement = false;
	private Reglage etat = Reglage.PRELAVAGE;
	private Boolean hublot = false;
	private Boolean liquide = false;
	
	public void setReglage(Reglage reglage) {
		this.etat = reglage;
	}
	
	public Reglage getReglage() {
		return this.etat;
	}
	
	public Boolean stopStart() {
		this.fonctionnement = !this.fonctionnement;
		return this.fonctionnement;
	}
	
	public Boolean fermerHublot() {
		this.hublot = false;
		return this.hublot;
	}
	
	public Boolean ouvrirHublot() {
		this.hublot = !this.fonctionnement;
		return this.hublot;
	}
	
	public Boolean ajouterLiquide() {
		this.liquide = true;
		return this.liquide;
	}

}
