package interfaces;

public class TraducteurSansInterface {

	public String traduit(String mot) {
		switch(mot) {
		case "maison":
			return "house";
		case "homme":
			return "man";
		default:
			return "unknown";
		}
	}
}
