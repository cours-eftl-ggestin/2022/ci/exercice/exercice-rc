package interfaces;

public interface IReveil {

	public void setHeure(String heure);
	
	default void start() {
		System.out.println("Sonne comme un réveil");
	}
}
