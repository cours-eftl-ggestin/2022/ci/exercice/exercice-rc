package tableaux;

public class Tableau {

	private String[] tableau = new String[4];
	
	//private String[] tableau2 =  {"chaine1", "chaine2", "chaine3"};
	
	public void chargement() {
		System.out.println("chargement...");
		this.tableau[0] = "chaine1";
		this.tableau[1] = "chaine2";
		this.tableau[2] = "chaine3";
	}
	
	public void afficheTableau() {
		System.out.println("afficheTableau...");
		for (int i = 0 ; i < this.tableau.length ; i++) {
			System.out.println(this.tableau[i]);
		}
	}
	
	public void accesTableau() {
		System.out.println("accesTableau...");
		System.out.println(this.tableau[3]); // NULL
		System.out.println(this.tableau[4]); // Exception
	}
	
}
